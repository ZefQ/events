package logica.events.adapters;

import java.util.List;

import logica.events.holders.EventItem;
import android.content.Context;
import logica.events.main.R;
import android.util.Log;
import android.view.*;
import android.widget.*;

public class EventAdapter extends ArrayAdapter<EventItem> {
	
	int resource;
	String response;
	Context context;
	private LayoutInflater mInflater;
	
    public EventAdapter(Context context, int textViewResourceId, List<EventItem> items) {
		super(context, textViewResourceId, items);
		this.resource = textViewResourceId;
		LayoutInflater.from(context);
	}
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	LinearLayout itemView;
    	EventItem ei = getItem(position);
    	
    	Log.v(this.getClass().getName(), "Name: " + ei.getName()+ ", Comment " + ei.getDate()+ ", Time " + ei.getTime());
    	
    	if(convertView==null) {	//if view is null we have to inflate it
    		itemView = new LinearLayout(getContext());
    		String inflate = Context.LAYOUT_INFLATER_SERVICE;
			mInflater = (LayoutInflater) getContext().getSystemService(inflate);
			mInflater.inflate(resource, itemView, true);
    	}
    	else {
    		itemView = (LinearLayout) convertView;
    	}
    	
    	TextView eventName = (TextView)itemView.findViewById(R.id.txtEventName);
    	TextView eventDate = (TextView)itemView.findViewById(R.id.txtEventDate);
    	TextView eventTime = (TextView)itemView.findViewById(R.id.txtEventTime);
    	
    	eventName.setText(ei.getName());
    	eventDate.setText(ei.getDate());
    	eventTime.setText(ei.getTime());
    	
		return itemView;
    	
    }

}