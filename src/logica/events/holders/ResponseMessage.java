package logica.events.holders;

/**
 * Holder class for event, used to display a list of events
 * @author kempef
 */
public class ResponseMessage {
	private String status;
	private String message;
	
	public ResponseMessage(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return this.status;
	}

	public String getMessage() {
		return this.message;
	}
}