package logica.events.holders;

/**
 * Holder class for info about a event
 * @extends EventItem
 * @author kempef
 *
 */
public class EventInfoItem extends EventItem{
	private String info;
	
	public EventInfoItem(int id, String heading, String date, String time, String info) {
		super(id, heading, date, time);
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

}