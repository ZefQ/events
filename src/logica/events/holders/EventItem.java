package logica.events.holders;

/**
 * Holder class for event, used to display a list of events
 * @author kempef
 */
public class EventItem {
	private int id;
	private String heading;
	private String date;
	private String time;
	
	public EventItem(int id, String heading, String date, String time) {
		this.id = id;
		this.heading = heading;
		this.date = date;
		this.time = time;
	}

	public int getID() {
		return id;
	}
	
	public String getName() {
		return heading;
	}

	public String getDate() {
		return date;
	}

	public String getTime() {
		return time;
	}
}