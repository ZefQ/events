package logica.events.common;

import logica.events.holders.ResponseMessage;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class WebService {
	private static Gson g = new Gson();
	private static HttpClient client;	
	private static BasicResponseHandler br = new BasicResponseHandler();
	
	/**
	 * Send a POST request to server
	 * 
	 * @param URL The URL for the request
	 * @param msg The message to POST
	 * @return The servers responce as string
	 */
	public static String POST(String URL, String msg) {
		try {            
			HttpPost post = new HttpPost(URL);
			
			client = new DefaultHttpClient();
			StringEntity entity = new StringEntity(msg, "UTF-8");
		    entity.setContentType("application/json");
		    post.setEntity(entity);	     
			return client.execute(post, br);
		}
		catch (Exception ex) {
			return "POST ERROR: "+ex.getMessage(); //TODO throw error and let calling method handle it
		}
	}
	
	
	/**
	 * Send a GET request to server
	 * 
	 * @param URL The URL for the request
	 * @return The servers responce as string
	 */
	public static String GET(String URL) {
		try {
			client = new DefaultHttpClient();
	    	HttpGet get = new HttpGet(URL);
	    	BasicResponseHandler br = new BasicResponseHandler();
			return client.execute(get, br);
		} 
		catch(Exception ex) {
			return "GET ERROR: "+ex.getMessage(); //TODO throw error and let calling method handle it
		}
	}
	
	/**
     * Sends event responses to web service
     *  
     * @param response 1 or 0 (true or false)
     * @param pos The position of the event in the list, used to get the id of the event
     */
    public static void respond(Context mContext, int response, int id) {
    	try {
    		String url = String.format(Variables.EVENT_RESPONSE_URL, Variables.SETTINGS.name, id);
        	String sendJSON = "{\"accept\":"+response+"}";        	
        	String responseJSON = WebService.POST(url, sendJSON);	//Send information to service, and get response
        	ResponseMessage rm = g.fromJson(responseJSON, ResponseMessage.class);
        	       	
        	Toast.makeText(mContext, rm.getMessage(), Toast.LENGTH_SHORT).show();
    	} catch (Exception ex) {
    		Log.e("WEBSERVICE RESPONDE: ", "ERROR SEND RESPOND: "+ex.getMessage());
    	}
    }
}
