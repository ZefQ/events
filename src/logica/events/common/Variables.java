package logica.events.common;

import logica.events.holders.Settings;

public class Variables {
	//URL variables
	public static final String BASE_URL = "http://164.9.214.129:8080/";
	public static final String USER_EVENTS_URL = BASE_URL+"user/%s/event/";
	public static final String EVENT_INFO_URL = BASE_URL+"event/%s/info/";
	public static final String EVENT_RESPONSE_URL = USER_EVENTS_URL+"%s/";
	
	//accept variables
	//TODO change names to EVENT_ACCEPTED and so on
	public static final int ACCEPT = 1;
	public static final int DECLINE = 0;
	public static final int UNDECIDED = -1;
	
	//Ssettings variable
	public static final String SETTINGS_FILE = "settings";
	public static Settings SETTINGS;
}
