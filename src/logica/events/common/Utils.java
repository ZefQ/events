package logica.events.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;

public class Utils {

	
	/**
	 * Read a file stored from internal storage
	 * @param mContext Context
	 * @param filename String
	 * @return	content of file as String
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String readFile(Context mContext, String filename) throws FileNotFoundException, IOException {
		StringBuffer content = new StringBuffer("");
        
   		FileInputStream fis = mContext.openFileInput("settings");
    	int i;
    	while( (i = fis.read()) != -1)
    		content.append((char)i);
	    
    	fis.close();
    	return content.toString();
	}
}
