package logica.events.main;


import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SettingsActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        
    }
    
public void onSave(View view){	    	
		EditText caption = (EditText) findViewById(R.id.name);
		String name = caption.getText().toString();
				
		try {
			String FILENAME = "settings";
			String string = "{\"name\":\""+ name +"\"}";
			
			FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
			fos.write(string.getBytes());
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Intent i = new Intent(SettingsActivity.this, LogicaEventsActivity.class);
    	startActivity(i);
		this.finish();
		
    }
}