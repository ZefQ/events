package logica.events.main;

import logica.events.common.Variables;
import logica.events.common.WebService;
import logica.events.holders.EventInfoItem;
import logica.events.holders.ResponseMessage;
import logica.events.main.R;

import com.google.gson.Gson;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class EventInfoActivity extends Activity {
	
	int id;
	Gson g = new Gson();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_info);
        
        Bundle extras = getIntent().getExtras();	
        if(extras == null){
        	return;
        }            
        id = extras.getInt("ID");
        
        //get event information
		String responseJSON = WebService.GET(String.format(Variables.EVENT_INFO_URL, id));	//Get information from service
	    EventInfoItem eii = g.fromJson(responseJSON, EventInfoItem.class);
	    
	    //add info to GUI
	    TextView eventName = (TextView)findViewById(R.id.txtEventName);
    	TextView eventDate = (TextView)findViewById(R.id.txtEventDate);
    	TextView eventTime = (TextView)findViewById(R.id.txtEventTime);
    	TextView eventInfo = (TextView)findViewById(R.id.txtEventInfo);
		
    	eventName.setText(eii.getName());
    	eventDate.setText(eii.getDate());
    	eventTime.setText(eii.getTime());
    	eventInfo.setText(eii.getInfo());
	}
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) { 
		//create menu so it can be activated by menu press
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.event_info_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle menu action
        switch (item.getItemId()) {
            case R.id.accept:
            	WebService.respond(this, Variables.ACCEPT, id);
                return true;
            case R.id.decline:
            	WebService.respond(this, Variables.DECLINE, id);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
