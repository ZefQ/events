package logica.events.main;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import com.google.gson.*;

import logica.events.adapters.*;
import logica.events.common.*;
import logica.events.holders.*;
import logica.events.main.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

public class LogicaEventsActivity extends Activity {
    
	ListView lstV;
	EventAdapter eventAdapter;
	ArrayList<EventItem> eventItemArray;
	Gson g = new Gson();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        if(!loadSettings())
        	return;
        
        //setup list view
        lstV = (ListView)findViewById(R.id.lstV);
        eventItemArray = new ArrayList<EventItem>();
        eventAdapter = new EventAdapter(this, R.layout.event_list, eventItemArray);
        lstV.setAdapter(eventAdapter);
        
		//setup onclick handler
		lstV.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            	Intent i = new Intent(LogicaEventsActivity.this, EventInfoActivity.class);
				i.putExtra("ID", eventAdapter.getItem(position).getID());
				startActivity(i);
            }
        });
		
		registerForContextMenu(lstV);	//register list for context menu
		
    }
    
    
    /**
     * This one is called when activity is created and when the activite gets focus back
     */
    @Override
    public void onResume() {
    	super.onResume();
    	updateEventList();	// update eventItemArray with available events
    	Log.i("ON RESUME", "RESUME");
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    	//setup context menu so it can be activated by long press on a event
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }
    
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	//handle menu action
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        int id = eventAdapter.getItem(info.position).getID(); // get id of the selected item
        
        switch (item.getItemId()) {
            case R.id.accept:
            	WebService.respond(this, Variables.ACCEPT, id);
            	updateEventList();
                return true;
            case R.id.decline:
            	WebService.respond(this, Variables.DECLINE, id);
            	updateEventList();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) { 
		//create menu so it can be activated by menu press
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle menu action
        switch (item.getItemId()) {
            case R.id.showSettings:        	
            	Intent i = new Intent(LogicaEventsActivity.this, SettingsActivity.class);
            	startActivity(i);
            	this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    
    /**
     * load settings
     * 
     * if a settings file can't be found SettingsActivity is loaded so the user can set up necessary settings  
     */
    private boolean loadSettings() {     
        try {
        	//deleteFile(Variables.SETTINGS_FILE);
    		String content = Utils.readFile(this, Variables.SETTINGS_FILE);
    		Variables.SETTINGS = g.fromJson(content, Settings.class);
    		return true;
		} 
        catch (FileNotFoundException e) {
        	Intent i = new Intent(LogicaEventsActivity.this, SettingsActivity.class);
        	startActivity(i);
        	this.finish();
		}
        catch (IOException ex) {
        	Log.e("MAIN:", ex.getMessage());
        }
        return false;
    }
    
    
    private void updateEventList() {
    	//remove any elements already in eventItemArray and update list
    	eventItemArray.clear();	
    	eventAdapter.notifyDataSetChanged();
    	
    	//get events
        Gson g = new Gson();       
    	String responseJSON = WebService.GET(String.format(Variables.USER_EVENTS_URL, Variables.SETTINGS.name));	//Get information from service

    	//if there is no new events for user the server will return a message, gson will throw a exception, we catch it and displayt de response to the user
    	try {
	    	EventItem[] ei = g.fromJson(responseJSON, EventItem[].class);
			
		    //add events to array
			for(EventItem event : ei){
				eventItemArray.add(event);
			}				    		    	    
		    
			//update list view
			eventAdapter.notifyDataSetChanged();
    	} 
    	catch(JsonSyntaxException ex) {
    		ResponseMessage rm = g.fromJson(responseJSON, ResponseMessage.class);
    		Toast.makeText(getApplicationContext(), rm.getMessage(), Toast.LENGTH_SHORT).show();
    	}
    }
}